FROM mcr.microsoft.com/playwright/python:v1.37.0-jammy

# Desactivation du reglage creneaux horaire
ARG DEBIAN_FRONTEND=noninteractive

# Mise à jour ubuntu
RUN apt-get -y update \
&& apt-get -y upgrade

# Installation outil pour debug
RUN apt install -y curl net-tools

# Installation de playwright
RUN pip install pytest \
    pytest-playwright

# Installation navigateur pour playwright
RUN playwright install chrome \
    firefox
